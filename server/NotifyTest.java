import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JTextArea;

public class NotifyTest extends JFrame {

    String title;
    String msg;

    public static void main(String[] args) {
        if (args != null && args.length > 1) {
            for (String string : args) {
                System.out.println(string);
            }
            new NotifyTest(args[0], args[1]);
        }
    }

    public NotifyTest(String title, String msg) {
        setSize(300, 300);
        setTitle(title);

        // JTextField jt = new JTextField("fffffffffff");
        // jt.setFont(new Font("宋体", Font.BOLD, 20));
        // add(jt);

        JTextArea text = new JTextArea();
        text.setText(msgPro(msg));
        text.setEditable(false);
        text.setLineWrap(true);
        text.setFont(new Font("宋体", Font.BOLD, 20));
        add(text);

        //
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        setSize(300, 300);

        int windowWidth = getWidth(); //
        int windowHeight = getHeight(); //
        Toolkit kit = Toolkit.getDefaultToolkit(); //

        int screenHeight = kit.getScreenSize().height; //
        setLocation(80, screenHeight - windowWidth);//

        setVisible(true);
    }

    public String msgPro(String msgs) {
        StringBuilder sb = new StringBuilder();
        String[] strings = msgs.split("-");
        for (String string : strings) {
            sb.append("   ");
            sb.append(string);
            sb.append("\n");
        }
        return sb.toString();
    }
}

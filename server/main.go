package main

import (
	"fmt"

	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	// "strconv"
	"time"
	// "strings"
)

func main() {
	ip_internal()
	http.HandleFunc("/notify", notify)       //设置访问的路由
	err := http.ListenAndServe(":9394", nil) //设置监听的端口
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func ip_internal() {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops:" + err.Error())
		os.Exit(1)
	}
	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				os.Stdout.WriteString(ipnet.IP.String() + "\n")
			}
		}
	}
}

func notify(w http.ResponseWriter, r *http.Request) {
	time := time.Now().Format("2006-01-02 15:04:05")
	fmt.Println(time)
	r.ParseForm()
	fmt.Println("range")
	for k, v := range r.Form {
		fmt.Println(k, v)
	}
	tp := r.FormValue("type")
	msg := r.FormValue("msg")
	go notifyJava(tp, msg)
	fmt.Fprintf(w, `{"code":0,"message":"succ"}`)
}

func notifyJava(tp string, msg string) {
	c := exec.Command("java", "NotifyTest", tp, msg)
	if err := c.Run(); err != nil {
		fmt.Println("Error: ", err)
	}
}

package top.crossrun.notify;

interface NotifyServiceInterface {
    void setCheckPhone(boolean checkPhone);

    void setCheckMessage(boolean checkMessage);

    void setCheckNotification(boolean checkNotification);
}
